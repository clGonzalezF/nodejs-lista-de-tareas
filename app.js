const argv = require('./config/yargs').argv;
const colors = require('colors');
const pendientes = require('./pendientes/pendientes');

let comando = argv._[0];

//console.log(argv);

switch (comando) {
    case 'crear':
        let tarea = pendientes.crear(argv.descripcion);
        console.log(tarea);
        break;
    case 'listar':
        let tareas = pendientes.cargarDB();
        if (tareas.length > 0) {
            console.log(colors.green('====== Lista de tareas ======='));
            for (let value of tareas) {
                console.log('Descripcion: ' + value.descripcion);
                console.log('L Estado: ' + value.completado);
            }
            console.log(colors.green('=============================='));
        } else {

        }
        break;
    case 'actualizar':
        let actualizado = pendientes.actualizar(argv.descripcion, argv.completado);
        if (actualizado == true) {
            console.log(`Tarea "${ argv.descripcion }" actualizada > ${ argv.completado }`);
        } else {
            console.log('La tarea no fue encontrada'.red);
        }
        break;
    case 'borrar':
        let borrar = pendientes.borrar(argv.descripcion, argv.completado);
        if (borrar == true) {
            console.log(`Tarea "${ argv.descripcion }" fue eliminada`);
        } else {
            console.log('La tarea no fue encontrada'.red);
        }
        break;
    default:
        console.log(`Comando < ${ comando } > no existe`);
}