const fs = require('fs');
const colors = require('colors');
let nameDB = 'db/data.json';

let listaPendientes = [];

const cargarDB = () => {
    try {
        listaPendientes = require('../db/data.json');
    } catch (error) {
        listaPendientes = [];
    }
    return listaPendientes;
}

const guardarDB = () => {
    let data = JSON.stringify(listaPendientes);

    fs.writeFile(nameDB, data, (err) => {
        if (err) throw new Error('No fue posible guardar');
        //console.log(colors.bgYellow(':: Pendiente guardada :: '));
    });
}

const crear = (descripcion) => {
    cargarDB();
    let pendiente = {
        descripcion,
        completado: false
    };
    listaPendientes.push(pendiente);
    guardarDB();
    return pendiente;
};

const actualizar = (descripcion, completado = true) => {
    cargarDB();
    let index = listaPendientes.findIndex(tarea => {
        return tarea.descripcion === descripcion;
    });
    if (index >= 0) {
        listaPendientes[index].completado = completado;
        guardarDB();
        return true;
    } else {
        return false;
    }
}

const borrar = (descripcion) => {
    cargarDB();
    let nuevaLista = listaPendientes.filter(tarea => {
        return tarea.descripcion !== descripcion;
    })
    if (listaPendientes.length !== nuevaLista.length) {
        listaPendientes = nuevaLista;
        guardarDB();
        return true;
    } else {
        return false;
    }
}

module.exports = {
    crear,
    cargarDB,
    actualizar,
    borrar
}