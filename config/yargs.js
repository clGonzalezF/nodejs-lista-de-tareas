const yargs = require('yargs');

const descripcion = {
    demand: true,
    alias: 'd',
    desc: 'Descripcion de la tarea por hacer'
};
const completado = {
    default: true,
    alias: 'c',
    desc: 'Marca como completado o pendiente la tarea'
}

let argv = yargs
    .command('crear', 'Crear un elemento por hacer', { descripcion })
    .command('listar', 'Muestra las tareas actuales')
    .command('actualizar', 'Actualizar el estado completado de una tarea', {
        descripcion,
        completado
    })
    .command('borrar', 'Boora una tarea', { descripcion })
    .help().argv;

module.exports = {
    argv
}